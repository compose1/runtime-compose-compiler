import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.31"
    id("org.jetbrains.compose") version "0.3.2"
    id("com.github.gmazzo.buildconfig") version "3.0.0"
}

group = "de.appsonair.compose"
version = "1.0"

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
}

dependencies {
    implementation(compose.desktop.currentOs)
    implementation(kotlin("compiler-embeddable"))
    implementation(kotlin("reflect"))
}

afterEvaluate {
    val pluginClasspath = configurations.kotlinCompilerPluginClasspath
    val pluginPath = pluginClasspath.get().files.map { it.toString() }.joinToString(",")
    println(pluginPath)
    buildConfig.buildConfigField("String", "PLUGIN_PATH", """"$pluginPath"""")
    val kotlinClasspath = configurations.kotlinCompilerClasspath
    val path = kotlinClasspath.get().files.map { it.toString() }.joinToString(",")
    buildConfig.buildConfigField("String", "KOTLIN_PATH", """"$path"""")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}
