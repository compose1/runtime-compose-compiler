package runtime_class_loading

import java.io.File
import kotlin.reflect.KClass

/**
 * apps on air
 *
 * @author Timo Drick
 */

inline fun <reified T: I, I: Any>loadClass(path: File) = loadClass<T, I>(path, T::class)

/**
 * We can not return the class itself because after reloading it is another type.
 * So only an interface can be returned that is loaded in the parent class loader.
 */
@Suppress("UNCHECKED_CAST")
fun <T: I, I: Any>loadClass(path: File, clazz: KClass<T>): I {
    val className = checkNotNull(clazz.qualifiedName)
    val classLoader = CompiledScriptClassLoader(
        parent = clazz.java.classLoader,
        path = path.absoluteFile
    )
    val reloadedClazz = classLoader.loadClass(className).kotlin
    return reloadedClazz.constructors.first().call() as I
}

class CompiledScriptClassLoader(parent: ClassLoader, private val path: File) : ClassLoader(parent) {
    override fun loadClass(name: String, resolve: Boolean): Class<*> = if (class2File(name).exists()) {
        val bytes = class2File(name).readBytes()
        defineClass(name, bytes, 0, bytes.size)
    } else {
        parent.loadClass(name)
    }
    private fun class2File(className: String): File {
        val classFileName = className.replace(".", "/") + ".class"
        return File(path, classFileName)
    }
}

