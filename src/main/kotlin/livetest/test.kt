package livetest

import ILive
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

/**
 * apps on air
 *
 * @author Timo Drick
 */
class LiveTest : ILive {
    @Composable
    override fun testMethod() {
        Column(Modifier.fillMaxSize()) {
            Text("test method")
            println("test method called")
            testFunction1(Modifier.fillMaxWidth())
        }
    }
}

@Composable
fun testFunction1(modifier: Modifier) {
    Text("Test function")
}
