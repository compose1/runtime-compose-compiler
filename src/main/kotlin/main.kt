import androidx.compose.desktop.Window
import androidx.compose.runtime.Composable
import de.appsonair.compose.RuntimeComposeCompiler.BuildConfig
import livetest.LiveTest
import org.jetbrains.kotlin.cli.common.arguments.K2JVMCompilerArguments
import org.jetbrains.kotlin.cli.common.messages.MessageRenderer
import org.jetbrains.kotlin.cli.common.messages.PrintingMessageCollector
import org.jetbrains.kotlin.cli.jvm.K2JVMCompiler
import org.jetbrains.kotlin.config.Services
import runtime_class_loading.loadClass
import java.io.File

interface ILive {
    @Composable
    fun testMethod()
}

fun main() {
    val classPath = System.getProperty("java.class.path")
        .split(System.getProperty("path.separator"))
        .filter {
            File(it).exists() && File(it).canRead()
        }.joinToString(":")
    println(classPath)
    File("runtime").mkdir()
    val compilerArgs = K2JVMCompilerArguments().apply {
        freeArgs = listOf("test/test.kt")
        destination = "runtime"
        classpath = classPath
        noReflect = true
        noStdlib = true
        jvmTarget = "11"
        useIR = true
        //pluginClasspaths = arrayOf("compiler-0.3.2.jar")
        pluginClasspaths = BuildConfig.PLUGIN_PATH.split(",").toTypedArray()
        script = false
        noOptimize = true
        reportPerf = true
    }
    val messageCollector = PrintingMessageCollector(System.err, MessageRenderer.PLAIN_RELATIVE_PATHS, false)
    val start = System.currentTimeMillis()
    val compiler = K2JVMCompiler()
    val result = compiler.exec(messageCollector, Services.EMPTY, compilerArgs)
    println(result)
    val duration = System.currentTimeMillis() - start
    println("Compile time: $duration ms")
    val loadedClass = loadClass<LiveTest, ILive>(File("runtime"))
    Window {
        loadedClass.testMethod()
    }
}
