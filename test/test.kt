package livetest

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import ILive

/**
 * apps on air
 *
 * @author Timo Drick
 */
class LiveTest : ILive {
    @Composable
    override fun testMethod() {
        Column(Modifier.fillMaxSize()) {
            Text("injected test method")
            println("injected test method called")
            testFunction1(Modifier.fillMaxWidth())
        }
    }
}

@Composable
fun testFunction1(modifier: Modifier) {
    Text("injected Test function", modifier = modifier)

}
